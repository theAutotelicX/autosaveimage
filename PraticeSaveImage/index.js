﻿const electron = require('electron');
const app = electron.app;

const BrowserWindow = electron.BrowserWindow;
//grabContent.getElement1();

function createWindow() {
    let mainWindow;
    mainWindow = new BrowserWindow({
        width: 400,
        height: 500,
        //frame: false
		webPreferences: {
			nodeIntegration: true
		}
    });
    
    //mainWindow.loadURL('https://github.com');
    ///local
    let url = require('url').format({
        protocol: 'file',
        slashes: true,
        pathname: require('path').join(__dirname, 'uiForm.html')
    });
	mainWindow.webContents.openDevTools()
    
    mainWindow.loadURL(url);
    //mainWindow.show();
}

app.on('ready', createWindow);
app.on('window-all-closed',()=>{
	app.quit();
});